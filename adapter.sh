#!/bin/bash

# this needs sudo access...

COM1=$1
COM2=$2

install() {
	curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
	sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
	sudo apt-get update && sudo apt-get install -y nomad consul
	if [ "$COM2" == "server" ]; then
		sudo wget \
		https://github.com/fabiolb/fabio/releases/download/v1.5.14/fabio-1.5.14-go1.15-linux_amd64 \
		-O /usr/local/bin/fabio
	fi
}

start() {
	case $COM2 in
	'server')
		echo "Starting server"
		SERVER_DIR="/etc/nunet-adapter/server"
		if [ ! -d $SERVER_DIR ]; then
		  sudo mkdir -p $SERVER_DIR
		fi

		sudo cp ./configs/server.hcl $SERVER_DIR/
		sudo cp ./configs/nomad-server.service /etc/systemd/system/
		sudo cp ./configs/consul-server.service /etc/systemd/system/
		sudo cp ./configs/fabio-server.service /etc/systemd/system/

		sudo systemctl enable nomad-server.service
		sudo systemctl enable consul-server.service
		sudo systemctl enable fabio-server.service
		sudo systemctl start nomad-server.service
		sudo systemctl start consul-server.service
		sudo systemctl start fabio-server.service

		;;

	'client')
		echo "Starting client"
		if [ ! -d /etc/nunet-adapter/client ]; then
		  sudo mkdir -p /etc/nunet-adapter/client
		fi

		CLIENT_DIR="/etc/nunet-adapter/client"
		if [ ! -d $CLIENT_DIR ]; then
		  sudo mkdir -p $CLIENT_DIR
		fi

		sudo cp ./configs/client.hcl $CLIENT_DIR/
		sudo cp ./configs/nomad-client.service /etc/systemd/system/
		sudo cp ./configs/consul-client.service /etc/systemd/system/

		sudo systemctl enable nomad-client.service
		sudo systemctl enable consul-client.service
		sudo systemctl start nomad-client.service
		sudo systemctl start consul-client.service

		;;

	'office-client')
		echo "Starting client"
		if [ ! -d /etc/nunet-adapter/client ]; then
		  sudo mkdir -p /etc/nunet-adapter/client
		fi

		CLIENT_DIR="/etc/nunet-adapter/client"
		if [ ! -d $CLIENT_DIR ]; then
		  sudo mkdir -p $CLIENT_DIR
		fi

		sudo cp ./configs/office-desktop.hcl $CLIENT_DIR/
		sudo cp ./configs/nomad-client-office.service /etc/systemd/system/
		sudo cp ./configs/consul-client-office.service /etc/systemd/system/

		sudo systemctl enable nomad-client-office.service
		sudo systemctl enable consul-client-office.service
		sudo systemctl start nomad-client-office.service
		sudo systemctl start consul-client-office.service

		;;

	'nunetio.ddns.net')
		echo "Starting client"

		CLIENT_DIR="/etc/nomad"
		if [ ! -d $CLIENT_DIR ]; then
		  sudo mkdir -p $CLIENT_DIR
		fi

		sudo cp ./configs/client-nunetio.ddns.net.hcl $CLIENT_DIR/nomad-client.hcl
		sudo cp ./configs/nomad-client.service /etc/systemd/system/

		sudo systemctl enable nomad-client.service
		sudo systemctl start nomad-client.service

		;;

	'demo.nunet.io')
		echo "Starting client"

		CLIENT_DIR="/etc/nomad"
		if [ ! -d $CLIENT_DIR ]; then
		  sudo mkdir -p $CLIENT_DIR
		fi

		sudo cp ./configs/client-demo.nunet.io.hcl $CLIENT_DIR/nomad-client.hcl
		sudo cp ./configs/nomad-client.service /etc/systemd/system/

		sudo systemctl enable nomad-client.service
		sudo systemctl start nomad-client.service

		;;

	'dev')
		echo "Starting Dev"
		DEV_DIR="/etc/nunet-adapter/dev"
		if [ ! -d $DEV_DIR ]; then
		  sudo mkdir -p $DEV_DIR
		fi

		stop server
		stop client

		sudo cp ./configs/dev.hcl $DEV_DIR/
		sudo cp ./configs/nomad-dev.service /etc/systemd/system/


		sudo systemctl enable nomad-dev.service
		sudo systemctl start nomad-dev.service

		;;
	'test-client')
		echo "Starting Test Client"
		TEST_CLIENT_DIR="/etc/nunet-adapter/test-client"
		if [ ! -d $TEST_CLIENT_DIR ]; then
          echo "----------------"
		  sudo mkdir -p $TEST_CLIENT_DIR
		fi

		stop test-client

		sudo cp ./configs/test-client.hcl $TEST_CLIENT_DIR/
		sudo cp ./configs/nomad-test-client.service /etc/systemd/system/
		sudo cp ./configs/consul-test-client.service /etc/systemd/system/


		sudo systemctl enable nomad-test-client.service
		sudo systemctl enable consul-test-client.service
		sudo systemctl start nomad-test-client.service
		sudo systemctl start consul-test-client.service

		;;
	esac
}

stop() {
	case $COM2 in
		'server')
			echo "Stopping Nomad Server"
			sudo systemctl stop nomad-server.service
			sudo systemctl stop consul-server.service
			sudo systemctl stop fabio-server.service
			;;
		'client')
			echo "Stopping Nomad Client"
			sudo systemctl stop nomad-client.service
			sudo systemctl stop consul-client.service
			;;
		'office-client')
			echo "Stopping Nomad Office Client"
			sudo systemctl stop nomad-client-office.service
			sudo systemctl stop consul-client-office.service
			;;
		'dev')
			echo "Stopping Nomad Dev"
			sudo systemctl stop nomad-dev.service
			;;
		'test-client')
			echo "Stopping Nomad/Consul Test Client"
			sudo systemctl stop nomad-test-client.service
			sudo systemctl stop consul-test-client.service
			;;
	esac
}


restart() {
	case $COM2 in
		'server')
			echo "Restarting Nomad Server"
			sudo systemctl restart nomad-server.service
			sudo systemctl restart consul-server.service
			sudo systemctl restart fabio-server.service
			;;
		'client')
			echo "Restarting Nomad Client"
			sudo systemctl restart nomad-client.service
			sudo systemctl restart consul-client.service
			;;
		'office-client')
			echo "Restarting Nomad Office Client"
			sudo systemctl restart nomad-client-office.service
			sudo systemctl restart consul-client-office.service
			;;
		'dev')
			echo "Restarting Nomad Dev"
			sudo systemctl restart nomad-dev.service
			;;
		'test-client')
			echo "Restarting Nomad/Consul Test Client"
			sudo systemctl restart nomad-test-client.service
			sudo systemctl restart consul-test-client.service
			;;
	esac
}

uninstall() {
	case $COM2 in
		'server')
			echo "Uninstalling Nomad Server"
			stop server
			sudo systemctl disable nomad-server.service
			sudo systemctl disable consul-server.service
			sudo systemctl disable fabio-server.service
			;;
		'client')
			echo "Uninstalling Nomad Client"
			stop client
			sudo systemctl disable nomad-client.service
			sudo systemctl disable consul-client.service
			;;
		'office-client')
			echo "Uninstalling Nomad Office Client"
			sudo systemctl disable nomad-client_office.service
			sudo systemctl disable consul-client_office.service
			;;
		'dev')
			echo "Uninstalling Nomad Dev"
			stop dev
			sudo systemctl disable nomad-dev.service
			;;
		'test-client')
			echo "Uninstalling Nomad/Consul Test Client"
		    TEST_CLIENT_DIR="/etc/nunet-adapter/test-client"
         	if [ -d $TEST_CLIENT_DIR ]; then
		      sudo rm -rf $TEST_CLIENT_DIR
		    fi
			stop test-client
			sudo systemctl disable nomad-test-client.service
			sudo systemctl disable consul-test-client.service

            sudo rm -v /etc/systemd/system/nomad-test-client.service
		    sudo rm -v /etc/systemd/system/consul-test-client.service
			;;
	esac
}

status() {
	case $COM2 in
		'server')
			echo "Status: Nomad/Consul Server"
			stop server
			sudo systemctl status nomad-server.service
			sudo systemctl status consul-server.service
			sudo systemctl status fabio-server.service
			;;
		'client')
			echo "Status: Nomad/Consul Client"
			stop client
			sudo systemctl status nomad-client.service
			sudo systemctl status consul-client.service
			;;
		'office-client')
			echo "Status: Nomad/Consul Office Client"
			sudo systemctl status nomad-client_office.service
			sudo systemctl status consul-client_office.service
			;;
		'dev')
			echo "Status: Nomad Dev"
			stop dev
			sudo systemctl status nomad-dev.service
			;;
		'test-client')
			echo "Status: Nomad/Consul Test Client"
			sudo systemctl status nomad-test-client.service
			sudo systemctl status consul-test-client.service
			;;
	esac
}


case $COM1 in
	'start')
		echo "Starting nunet-adapter"
		start
		;;
	'stop')
		stop
		;;
	'restart')
		restart
		;;
	'status')
		status
		;;
	'install')
		install
		;;
	'uninstall')
		uninstall
		;;
esac


exit 0
