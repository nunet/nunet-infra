# Ai Dsl Test

## Clone repos

``` 
    bash clone.sh all
```

## Build docker images
``` 
    bash build.sh all

```

## Run services
``` 
    export db_name=
    export user_name=
    export password=
    
    bash up.sh all

```
(see actually exported values here: https://gitlab.com/nunet/integration-tests/-/issues/25)

## make call to adapter1


``` 
    docker exec -it nunet-adapter-news-score-ai-dsl bash -c "python3 test_ai_dsl.py"

```
