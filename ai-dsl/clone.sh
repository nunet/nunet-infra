#!/bin/bash

if [ -d "/home/$USER/ai-dsl" ] 
then
    echo "Directory /home/$USER/ai-dsl/ exists." 
else
    mkdir /home/$USER/ai-dsl
fi

cd /home/$USER/ai-dsl

nunet-adapter() {
    if [ -d "/home/$USER/ai-dsl/nunet-adapter" ] 
    then
        echo "Directory /home/$USER/ai-dsl/nunet-adapter exists." 
    else
        git clone https://gitlab.com/nunet/nunet-adapter.git
    fi
}



fake-news-score() {
    if [ -d "/home/$USER/ai-dsl/fake_news_score" ] 
    then
        echo "Directory /home/$USER/ai-dsl/fake_news_score exists." 
    else
        git clone https://gitlab.com/nunet/fake-news-detection/fake_news_score.git
    fi
}

uclnlp() {
    if [ -d "/home/$USER/ai-dsl/uclnlp" ] 
    then
        echo "Directory /home/$USER/ai-dsl/uclnlp exists." 
    else
        git clone https://gitlab.com/nunet/fake-news-detection/uclnlp.git
    fi
}

athene() {
    if [ -d "/home/$USER/ai-dsl/athene" ] 
    then
        echo "Directory /home/$USER/ai-dsl/athene exists." 
    else
        git clone https://gitlab.com/nunet/fake-news-detection/athene.git
    fi
}

tokenomics() {
    if [ -d "/home/$USER/ai-dsl/tokenomics-api-eth" ] 
    then
        echo "Directory /home/$USER/ai-dsl/tokenomics-api-eth exists." 
    else
        git clone https://gitlab.com/nunet/tokenomics-api-eth.git
    fi
    
}


all() {
    nunet-adapter
    athene
    uclnlp
    fake-news-score
    tokenomics
}

help () {
    echo "Usage: bash clone.sh OPTION [SERVICE]"
    echo "  Options:"
    echo "    nunet-adapter        clone nunet-adapter "
    echo "    fake-news-score        clone fake-news-score "    
    echo "    uclnlp        clone uclnlp "
    echo "    athene        clone athene "
    echo "    tokenomics        clone tokenomics "
    echo "    all        clone all "

}

case $1 in
    nunet-adapter) nunet-adapter ;;
    fake-news-score) fake-news-score ;;
    uclnlp) uclnlp ;;
    athene) athene ;;
    all) all;;
    tokenomics) tokenomics ;;
  *) help ;;
esac
