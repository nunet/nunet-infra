#!/bin/bash

nunet-adapter-news-score() {
    NAME=nunet-adapter-news-score-ai-dsl

    if [ -z ${NAME} ]
    then
      echo "no container with name ${NAME} is running"
    else
        docker container stop ${NAME} && docker container rm ${NAME}
    fi
    
    docker run  -d  \
    -e user_name=$user_name \
    -e db_name=$db_name \
    -e password=$password \
    -e tokenomics_api_name="testing-tokenomics" \
    -it --network host --name=$NAME nunet-adapter-ai-dsl bash \
    -c 'python3 nunet_adapter.py 6561'
}


nunet-adapter-uclnlp() {
    NAME=nunet-adapter-uclnlp-ai-dsl

    if [ -z ${NAME} ]
    then
      echo "no container with name ${NAME} is running"
    else
        docker container stop ${NAME} && docker container rm ${NAME}
    fi
    docker run  -d  \
    -e user_name=$user_name \
    -e db_name=$db_name \
    -e password=$password \
    -e tokenomics_api_name="testing-tokenomics" \
    -it --network host --name=$NAME nunet-adapter-ai-dsl bash \
    -c 'python3 nunet_adapter.py 6562'
}

nunet-adapter-athene() {

    NAME=nunet-adapter-athene-ai-dsl

    if [ -z ${NAME} ]
    then
      echo "no container with name ${NAME} is running"
    else
        docker container stop ${NAME} && docker container rm ${NAME}
    fi

    docker run  -d  \
    -e user_name=$user_name \
    -e db_name=$db_name \
    -e password=$password \
    -e tokenomics_api_name="testing-tokenomics" \
    -it --network host --name=$NAME nunet-adapter-ai-dsl bash \
    -c 'python3 nunet_adapter.py 6563'

}


fake-news-score() {
    NAME=news-score-ai-dsl

    if [ -z ${NAME} ]
    then
      echo "no container with name ${NAME} is running"
    else
        docker container stop ${NAME} && docker container rm ${NAME}
    fi
    docker run -d  \
        -e nunet_adapter_name="testing-nunet-adapter-news-score" -e NOMAD_PORT_rpc="6571" \
        -it --network host --name=$NAME news-score-ai-dsl  bash \
        -c 'python3 run_fake_news_score.py  --no-daemon'
}

uclnlp() {
    NAME=uclnlp-ai-dsl

    if [ -z ${NAME} ]
    then
      echo "no container with name ${NAME} is running"
    else
        docker container stop ${NAME} && docker container rm ${NAME}
    fi

    docker run  -d -e nunet_adapter_name="testing-nunet-adapter-uclnlp" -e NOMAD_PORT_rpc="6572"\
        -it --network host --name=$NAME uclnlp-ai-dsl bash \
        -c 'python3 run_uclnlp_service.py  --no-daemon '
}

athene() {    
    NAME=athene-ai-dsl

    if [ -z ${NAME} ]
    then
      echo "no container with name ${NAME} is running"
    else
        docker container stop ${NAME} && docker container rm ${NAME}
    fi

    docker run -d -e athene_system_name="testing-athene-system" -e nunet_adapter_name="testing-nunet-adapter-athene" -e NOMAD_PORT_rpc="6573" \
        -it --network host --name=$NAME athene-ai-dsl bash \
        -c 'python3 run_athene_service.py  --no-daemon'
    

    NAME=athene-system-ai-dsl

    if [ -z ${NAME} ]
    then
      echo "no container with name ${NAME} is running"
    else
        docker container stop ${NAME} && docker container rm ${NAME}
    fi

    docker run -d -e NOMAD_PORT_rpc="6574" \
        -it --network host --name=$NAME athene-system-ai-dsl bash \
        -c 'bash /root/athene_system/fnc/run.sh 6574'

}

tokenomics() {    
    NAME=tokenomics-ai-dsl

    if [ -z ${NAME} ]
    then
      echo "no container with name ${NAME} is running"
    else
        docker container stop ${NAME} && docker container rm ${NAME}
    fi
    docker run -d  -e rpc_address="http://135.181.222.170:8545" -e CONFIG="TEST"  \
    -it --network host --name=$NAME tokenomics-ai-dsl bash \
    -c 'python3 tokenomics.py 6570'
    
}

all() {
    nunet-adapter-news-score
    nunet-adapter-uclnlp
    nunet-adapter-athene
    athene
    uclnlp
    fake-news-score
    tokenomics
}


help () {
    echo "Usage: bash up.sh OPTION [SERVICE]"
    echo "  Options:"
    
    echo "    nunet-adapter-news-score        up the nunet-adapter-news-score "
    echo "    nunet-adapter-uclnlp        up the nunet-adapter-uclnlp "
    echo "    nunet-adapter-athene        up the nunet-adapter-athene "

    echo "    fake-news-score        up the fake-news-score "    
    echo "    uclnlp        up the uclnlp "
    echo "    athene        up the athene "
    echo "    tokenomics        up the tokenomics"

    echo "    all        up all "

}

case $1 in
    nunet-adapter-news-score) nunet-adapter-news-score ;;
    nunet-adapter-uclnlp) nunet-adapter-uclnlp ;;
    nunet-adapter-athene) nunet-adapter-athene ;;
    fake-news-score) fake-news-score ;;
    uclnlp) uclnlp ;;
    athene) athene ;;
    tokenomics) tokenomics ;;
    all) all;;
  *) help ;;
esac
