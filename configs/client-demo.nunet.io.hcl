# Increase log verbosity
log_level = "DEBUG"

data_dir = "/var/lib/nomad_client"

name = "demo-nunet-io"

datacenter = "demo-nunet-io"

advertise {
     http = "195.201.197.25"
     rpc  = "195.201.197.25"
     serf = "195.201.197.25"
}

client {
    enabled = true
    servers = ["nomad.icog-labs.com:4647"]
}

plugin "raw_exec" {
  config {
    enabled = true
  }
}

plugin "docker" {
  config {
    gc {
      dangling_containers {
        enabled = true
      }
    }
  }
}
