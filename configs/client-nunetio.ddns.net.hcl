# Increase log verbosity
log_level = "DEBUG"

data_dir = "/var/lib/nomad_client"

name = "kabir-desktop"

datacenter = "nunetio-ddns-net"

# this is not good in principle -- it should be somehow
# resolved from the router or a query -- since these addresses are dynamic
# hopefully it will get resolved when we have nat punching with ipv8...
advertise {
     http = "109.88.2.12"
     rpc  = "109.88.2.12"
     serf = "109.88.2.12"
}

client {
    enabled = true
    servers = ["nomad.icog-labs.com:4647"]
}

plugin "raw_exec" {
  config {
    enabled = true
  }
}

plugin "docker" {
  config {
    gc {
      dangling_containers {
        enabled = true
      }
    }
  }
}
