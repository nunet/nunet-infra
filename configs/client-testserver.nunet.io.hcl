# Increase log verbosity
log_level = "DEBUG"

data_dir = "/var/lib/nomad_test_client"

name = "nunet test server"

datacenter = "testing-nunet-io"

advertise {
    http = "135.181.222.170"
    rpc = "135.181.222.170"
    serf = "135.181.222.170"
}

client {
    enabled = true
    servers = ["nomad.icog-labs.com:4647"]
}

plugin "raw_exec" {
  config {
    enabled = true
  }
}

plugin "docker" {
  config {
    gc {
      dangling_containers {
        enabled = false
      }
    }
  }
}
