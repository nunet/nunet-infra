# Increase log verbosity
log_level = "DEBUG"

data_dir = "/tmp/client_anton_10"

name = "nunet server"

datacenter = "nunet-io"

client {
    enabled = true
    servers = ["nomad.icog-labs.com:4647"]
    network_interface = "enp0s31f6"
}

plugin "raw_exec" {
  config {
    enabled = true
  }
}

plugin "docker" {
  config {
    gc {
      dangling_containers {
        enabled = false
      }
    }
  }
}
