# Device Onboarding script

This script exposes a basic device onboarding process. It installs the required components and sequentially asks the user how much to system resource to allocate to NuNet and also the users payment address. Before you proceed with the installation, a complete backup of your system is highly recommended.


## System requirement for the onboarding script to function as needed

we only require for you to specify CPU and RAM but your system must have the following requirements before you decide to onboard it.

- CPU - 2000 mhz
- RAM    - 4 GB
- DISK SPACE  - 10 GB

If you are using GPUs, especially for an ML use case, you'd need a different set of requirements:

- CPU - Multicore and >2 Ghz
- GPU - NVIDIA starting from 10 series with at least 8 GB(recommended) VRAM
- RAM - 16 GB
- DISK SPACE  - 50 GB

To install the required packages `sudo` access is required.

## Via direct script download

### Onboarding

The following commands clone the repo, install and setup your device for the onboarding process.

```
wget https://gitlab.com/nunet/nunet-infra/-/raw/master/device_onboarding/nunet-onboarding.sh
sudo bash nunet-onboarding.sh setup
```


### Uninstalling the platform

```
sudo bash nunet-onboarding.sh remove
```

If you choose to, you can also remove [Docker](https://www.docker.com) and [Nomad](https://www.nomadproject.io) that were installed during onboarding:

```
sudo apt remove docker-ce docker-ce-cli containerd.io nomad
```


## Via manual clone of the repository

You can also download the complete `nunet-infra` repository which contains device onboarding script.

1. Clone this repository to your machine

```
git clone https://gitlab.com/nunet/nunet-infra.git
```

OR
    
   If you want to use the develop branch(includes NVIDIA GPU onboarding support), use:

```
git clone -b develop https://gitlab.com/nunet/nunet-infra.git
```


2. go to the device onbording directory

```
cd nunet-infra/device_onboarding
```

3. download and install nomad, docker, python, podman and the requirements to run the python script.

```
sudo bash onboarding.sh install
```

4. generate device metadata and nomad client configuration.Then it install nomad configuration on the system. This option can also be used to update allocated compute resources and payment identity after first initialization.

```
sudo bash onboarding.sh config
```

### To confirm successful GPU onboarding

1. Look for your machine name when you run the following command:

 ```
 nomad node status
 ```

   We recommend you use assign your own name to your machine for familiarity. For example, _name-desktop_. 

2. Now run the same command but only with your machine's node _ID_:

```
nomad node status <ID>
```

   If you see your GPU listed under "Device Resource Utilization", then you have successfully onboarded your GPU on NuNet.

### To do all the process at once     
    
```
sudo bash onboarding.sh all
```

### To disconnect your device

```
sudo bash onboarding.sh pause
```

### To reconnect your device

```
sudo bash onboarding.sh unpause
```

### To remove your device and uninstall
    
```
sudo bash onboarding.sh remove
```

If you choose to, you can also remove [Docker](https://www.docker.com) and [Nomad](https://www.nomadproject.io) that were installed during onboarding:
```
sudo apt remove docker-ce docker-ce-cli containerd.io nomad
```

