import psutil
import time
import os
import json
import subprocess

import secrets
from pprint import pprint
from eth_keys import keys
from natdetector import get_ip_info


def generate_address(network):
    try:
        private_key = "{:64x}".format(secrets.randbits(256))
        private_key_bytes = bytes.fromhex(private_key)
        public_key_hex = keys.PrivateKey(private_key_bytes).public_key
        public_key_bytes = bytes.fromhex(str(public_key_hex)[2:])
        public_address = keys.PublicKey(public_key_bytes).to_address()
    except Exception as e:
        private_key = "{:64x}".format(secrets.randbits(256))
        private_key_bytes = bytes.fromhex(private_key)
        public_key_hex = keys.PrivateKey(private_key_bytes).public_key
        public_key_bytes = bytes.fromhex(str(public_key_hex)[2:])
        public_address = keys.PublicKey(public_key_bytes).to_address()

    client_info = {}
    client_info['private_key'] = private_key
    client_info['address'] = public_address
    client_info['deployment_network'] = network
    with open("./private/client_info.json", "w") as file:
        json.dump(client_info, file)
    print(f"private_key ==== {private_key}")
    print(f"address === {public_address}")
    return public_address


def generate_metadata(network, token_addr, reserved):
    metadata = {}
    metadata["name"] = os.environ["HOSTNAME"]
    total_resource = {}
    total_resource["update_timestamp"] = int(time.time())
    total_resource["ram_max"] = int(psutil.virtual_memory()[0] / 1024 / 1024)
    total_resource["total_cores"] = psutil.cpu_count(logical=True)
    cpufreq = psutil.cpu_freq()
    total_resource["cpu_max"] = cpufreq.max
    total_resource["cpu_usage"] = psutil.cpu_percent()
    metadata["resource"] = total_resource
    available_resources = {}
    available_resources["updated_timestamp"] = int(time.time())
    available_resources["ram"] = int(psutil.virtual_memory()[1] / 1024 / 1024)
    metadata["available"] = available_resources
    metadata["reserved"] = reserved
    metadata["network"] = network
    metadata["public_key"] = token_addr

    with open('./configs/client_config/metadata.json', 'w') as file:
        json.dump(metadata, file)


def generate_client_config():
    with open('./configs/client_config/metadata.json', 'r') as file:
        metadata = json.load(file)
    client_config = {}
    name = metadata["name"]
    if metadata["network"] == "nunet-private-alpha":
        datacenter = "nunet-private-alpha"
    elif metadata["network"] == "nunet-development":
        datacenter = "nunet-development"

    servers = ["nomad.nunet.io:4647"]
    client = {}
    host_volume = {}
    host_volume["machine-metadata"] = {
        "name": "machine-metadata",
        "path": "/etc/nunet"
    }
    client["host_volume"] = host_volume
    client["enabled"] = 'true'
    client["servers"] = servers
    client["reserved"] = metadata["reserved"]

    plugins = {}
    plugins["raw_exec"] = {
        "config": {
            "enabled": "true"
        }
    }
    plugins["docker"] = {
       
    }
    plugins["nomad-device-nvidia"] = {
         "config": {
                "enabled": "true",
                "fingerprint_period": "1m"
            }
    }

    plugins["nvidia"] = {
         "config": {
                "fingerprint_period": "5s"
            }
    }

    client_config["plugin"] = plugins

    client_config["log_level"] = "DEBUG"
    client_config["data_dir"] = "/var/log/nomad"
    client_config["plugin_dir"] = "/opt/nomad-plugin/"
    client_config["name"] = name
    client_config["datacenter"] = datacenter
    client_config["client"] = client
    with open('./configs/client_config/client.json', 'w') as file:
        json.dump(client_config, file)


if os.path.exists("./configs/client_config/metadata.json"):
    with open("./configs/client_config/metadata.json", "r+") as file:
        meta = json.load(file)

    if psutil.cpu_freq().max != 0:
        total_compute = psutil.cpu_freq().max*psutil.cpu_count(logical=True)
    else:
        total_compute = psutil.cpu_freq().current*psutil.cpu_count(logical=True)

    if int(psutil.virtual_memory()[0] / 1024 / 1024) >= 10000 and total_compute >= 6000:
        response = input("Do you want to run passive cardano node (Y/N)?\n")
        if response.lower() == "y":
            cardano_passive = "yes"
            try:
                file = open('/onboarding/configs/client_config/adapter-definition.json', 'r')
            except FileNotFoundError:
                file = open('./configs/adapter-definition.json', 'r')
            defn = json.load(file)
            print("To run passive cardano node you need to allocate at least 10000 MB of RAM and 6000 MHZ of CPU\n")    
            defn['Job']['TaskGroups'][0]["Tasks"][0]["Env"]["cardano_passive"] = cardano_passive
            with open('./configs/adapter-definition.json', 'w') as file:
                file.truncate(0)
                json.dump(defn,file)

    ram = input(
        f"Your system has {int(psutil.virtual_memory()[0] / 1024 / 1024)}MB of RAM. How much are you willing to allocate to NuNet in MB?\n"
    )
    cpu = input(
        f"Your system has {total_compute} MHz. How much are you willing "
        "to allocate to NuNet?\n"
    )
    while int(cpu) < 0 or int(cpu) > total_compute:
        print("Please enter a valid value.")
        cpu = input(
            f"Your system has {total_compute} MHz. How much are you willing "
            "to allocate to NuNet?\n"
        )
    network = input(
        "Select which network to join [nunet-private-alpha or nunet-development]?\n"
    )
    meta["network"] = network
    if network == "nunet-private-alpha":
        response = input("Do you have and existing payment address (Y/N)?")
        if response.lower() == "y":
            token_addr = input("Enter your payment address?\n")
        else:
            token_addr = generate_address(network)
    else:
        response = input("Do you have and existing payment address (Y/N)?")
        if response.lower() == "y":
            token_addr = input("Enter your payment address?\n")
        else:
            token_addr = generate_address(network)

    meta["public_key"] = token_addr

    reserved = {}

    reserved["cpu"] = (int(total_compute)) - int(cpu)
    reserved["memory"] = int(psutil.virtual_memory()[
        0] / 1024 / 1024) - int(ram)

    meta["reserved"] = reserved
    with open("./configs/client_config/metadata.json", "w") as file:
        file.truncate(0)
        json.dump(meta, file)
    generate_client_config()
    # print("Detecting NAT type ...")
    # subprocess.call(["python2", "natdetector.py", "save"])
    resp = input("Do you want to see the resulting configuration (Y/N)?\n")
    if resp.lower() == 'y':
        with open("./configs/client_config/metadata.json", "r") as file:
            meta = json.load(file)
        print("*********************************")
        print("\tMachine Metadata\t")
        print("*********************************")
        print("\n")
        pprint(meta)
    print("\n")
    pprint("Configuration files are saved in /etc/nunet")
    pprint("Private key is stored in /home/nunet. Keep it in a safe place.")

else:
    total_ram = int(psutil.virtual_memory()[0] / 1024 / 1024)
    if psutil.cpu_freq().max != 0:
        total_compute = psutil.cpu_freq().max*psutil.cpu_count(logical=True)
    else:
        total_compute = psutil.cpu_freq().current*psutil.cpu_count(logical=True)
    if int(psutil.virtual_memory()[0] / 1024 / 1024) >= 10000 and total_compute >= 6000:
        response = input("Do you want to run passive cardano node (Y/N)?\n")
        if response.lower() == "y":
            cardano_passive = "yes"
            try:
                file = open('/onboarding/configs/client_config/adapter-definition.json', 'r')
            except FileNotFoundError:
                file = open('./configs/adapter-definition.json', 'r')
            defn = json.load(file)
            print("To run passive cardano node you need to allocate at least 10000 MB of RAM and 6000 MHZ of CPU\n")    
            defn['Job']['TaskGroups'][0]["Tasks"][0]["Env"]["cardano_passive"] = cardano_passive
            with open('./configs/adapter-definition.json', 'w') as file:
                file.truncate(0)
                json.dump(defn,file)
    
    ram = input(
        f"Your system has {total_ram}MB of RAM. How much are you willing to allocate to NuNet in MB?\n"
    )
    cpu = input(
        f"Your system has {total_compute} MHz. How much are you willing "
        "to allocate to NuNet?\n"
    )
    while int(cpu) < 0 or int(cpu) > total_compute:
        print("Please enter a valid value.")
        cpu = input(
            f"Your system has {total_compute} MHz. How much are you willing "
            "to allocate to NuNet?\n"
        )
    network = input(
        "Select which network to join [nunet-private-alpha or nunet-development]?\n"
    )
    if network == "nunet-private-alpha":
        response = input("Do you have and existing payment address (Y/N)?")
        if response.lower() == "y":
            token_addr = input("Enter your payment address?\n")
        else:
            token_addr = generate_address(network)
    else:
        response = input("Do you have and existing payment address (Y/N)?")
        if response.lower() == "y":
            token_addr = input("Enter your payment address?\n")
        else:
            token_addr = generate_address(network)

    reserved = {}
    reserved["cpu"] = (int(total_compute)) - int(cpu)
    reserved["memory"] = total_ram - int(ram)

    generate_metadata(network, token_addr, reserved)
    generate_client_config()
    # print("Detecting NAT type ...")
    # subprocess.call(["python2", "natdetector.py", "save"])
    resp = input("Do you want to see the resulting configuration (Y/N)?")
    if resp.lower() == 'y':
        with open("./configs/client_config/metadata.json", "r") as file:
            meta = json.load(file)
        pprint(meta)
    pprint("Configuration files are saved in /etc/nunet")
    pprint("Private key is stored in /home/nunet. Keep it in a safe place.")


def config_adapter():
    client = json.load(open('./configs/client_config/client.json', 'r'))
    file = open('./configs/adapter-definition.json', 'r')
    defn = json.load(file)
    defn['Job']['Datacenters'] = [client["datacenter"]]

    if client["datacenter"] == "nunet-development":
        defn['Job']['Name'] = "testing-nunet-adapter-" + client["name"]
        defn['Job']['ID'] = "testing-nunet-adapter-" + client["name"]
        defn["Job"]["TaskGroups"][0]["Name"] = "testing-nunet-adapter-" + \
            client["name"]
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Name"] = "testing-nunet-adapter-" + client["name"]
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Services"][0]["Name"] = "testing-nunet-adapter-" + client["name"]
        if os.environ["OS_RELEASE"] == "raspbian":
            defn["Job"]["TaskGroups"][0]["Tasks"][0]["Config"]["image"] = "registry.gitlab.com/nunet/nunet-adapter:arm-test"
        else:
            defn["Job"]["TaskGroups"][0]["Tasks"][0]["Config"]["image"] = "registry.gitlab.com/nunet/nunet-adapter:test"
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Env"]["deployment_type"] = "test"
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Env"]["tokenomics_api_name"] = "testing-tokenomics"
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Env"]["LS_SERVICE_NAME"] = "testing-nunet-adapter-" + client["name"]        
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Services"][0]["Name"] = "testing-nunet-adapter-" + client["name"].replace("_","-")
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Services"][0]["Tags"][1] = "urlprefix/" + \
            "testing-nunet-adapter-" + client["name"] + " proto=grpc"
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Constraints"][0]["RTarget"] = client["name"]
        defn["Job"]["TaskGroups"][0]["Networks"][0]["ReservedPorts"][0]["Value"] = 60778
        os.environ['JOB_NAME'] = "testing-nunet-adapter-" + client["name"]
        with open('./configs/client_config/JOB_NAME.txt', 'w') as file:
            file.write("testing-nunet-adapter-" + client["name"])
    else:
        defn['Job']['Name'] = "nunet-adapter-" + client["name"]
        defn['Job']['ID'] = "nunet-adapter-" + client["name"]
        defn["Job"]["TaskGroups"][0]["Name"] = "nunet-adapter-" + client["name"]
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Name"] = "nunet-adapter-" + client["name"]
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Services"][0]["Name"] = "nunet-adapter-" + client["name"]
        if os.environ["OS_RELEASE"] == "raspbian":
            defn["Job"]["TaskGroups"][0]["Tasks"][0]["Config"]["image"] = "registry.gitlab.com/nunet/nunet-adapter:arm-latest"
        else:
            defn["Job"]["TaskGroups"][0]["Tasks"][0]["Config"]["image"] = "registry.gitlab.com/nunet/nunet-adapter:latest"
        
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Env"]["deployment_type"] = "prod"
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Env"]["tokenomics_api_name"] = "tokenomics"
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Env"]["LS_SERVICE_NAME"] = "nunet-adapter-" + client["name"]
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Services"][0]["Name"] = "nunet-adapter-" + client["name"].replace("_","-")
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Services"][0]["Tags"][1] = "urlprefix/" + \
            "nunet-adapter-" + client["name"] + " proto=grpc"
        defn["Job"]["TaskGroups"][0]["Tasks"][0]["Constraints"][0]["RTarget"] = client["name"]
        defn["Job"]["TaskGroups"][0]["Networks"][0]["ReservedPorts"][0]["Value"] = 60777
        os.environ['JOB_NAME'] = "nunet-adapter-" + client["name"]
        with open('./configs/client_config/JOB_NAME.txt', 'w') as file:
            file.write("nunet-adapter-" + client["name"])

    with open('./configs/client_config/adapter-definition.json', 'w') as file:
        file.truncate(0)
        json.dump(defn, file)


config_adapter()
