setup(){
    git clone https://gitlab.com/nunet/nunet-infra/ /tmp/nunet-infra
    cd /tmp/nunet-infra/
    git fetch --tags
    latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)
    git checkout $latestTag
    cd /tmp/nunet-infra/device_onboarding
    bash onboarding.sh install
    bash onboarding.sh config 
}


remove(){
    git clone https://gitlab.com/nunet/nunet-infra /tmp/nunet-infra
    cd /tmp/nunet-infra/device_onboarding
    bash onboarding.sh remove
}


all() {
    setup
    remove
}

case $1 in 
    setup) setup;;
    remove) remove;;
esac
