# this needs sudo access

get_distribution() {
    lsb_dist=""
    # Every system that we officially support has /etc/os-release
    if [ -r /etc/os-release ]; then
        lsb_dist="$(. /etc/os-release && echo "$ID")"
    fi
    # Returning an empty string here should be alright since the
    # case statements don't act unless you provide an actual value
    echo "$lsb_dist"
}

get_architecture() {
    # Used to determine whether the system is ARM64 or AMD64
    arch=""
    arch="$(dpkg --print-architecture)"
    echo "$arch"
}

install() {

    lsb_dist=$( get_distribution )
    lsb_dist="$(echo "$lsb_dist" | tr '[:upper:]' '[:lower:]')"
    arch=$( get_architecture )

    case "$lsb_dist" in

        ubuntu)

            case "$arch" in

                amd64)
                    sudo apt install software-properties-common -y
                    sudo apt-add-repository universe
                    sudo apt update
                    sudo apt install python2-minimal jq -y
                    sudo apt-get update
                    # nat
                    start_log
                    sudo apt-get -y install lsb-core
                    curl --version >/dev/null 2>&1
                    status=`echo $?`
                    if [[ $status -eq 0 ]]
                    then
                        echo "Curl is already installed on the system"
                    else
                        echo "Installing Curl"
                        sudo apt -y install  curl
                        echo "Installation Complete"
                    fi

                    nomad -v >/dev/null 2>&1
                    status=`echo $?`
                    if [[ $status -eq 0 ]]
                    then
                        echo "Nomad is already installed on the system"
                    else                
                        echo "-----Starting Nomad Installation-----"
                        curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
                        sudo apt-add-repository "deb [arch=$arch] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
                        sudo apt-get update && sudo apt-get -y install nomad
                        echo "Nomad installed successfully"
                    fi

                    docker -v >/dev/null 2>&1
                    status=`echo $?`
                    if [[ $status -eq 0 ]]
                    then 
                        echo "Docker is already installed on the system"
                    else
                        echo " -----Starting Docker Installation-----"
                        sudo apt-get purge docker-ce docker-ce-cli containerd.io
                        sudo apt-get update
                        sudo apt-get install -y apt-transport-https ca-certificates gnupg  
                        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
                        echo \
                        "deb [arch=$arch signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
                        $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
                        sudo apt-get update
                        sudo apt-get -y install docker-ce docker-ce-cli containerd.io
                        sudo groupadd docker
                        sudo usermod -aG docker $USER
                        echo "Docker installed successfully" 
                    fi


                    #Check for Secure Boot before proceeding with GPU driver installation, alert user with necessary information if yes.
                    sudo apt install -y mokutil
                    secure_boot_check="$(mokutil --sb-state | grep enabled)"
                    secure_boot="enabled"
                    if grep -q "$secure_boot" <<< "$secure_boot_check"; then
                        echo "************Important Note!!!************"
                        echo "Your system has UEFI Secure Boot enabled."
                        echo "*****************************************"
                        echo "a. NVIDIA GPU Driver installation will require you to enroll a password(confirmed twice) as a machine owner key(MOK)."
                        echo "b. Reboot your system after finishing the onboarding procedure."
                        echo "c. Make sure you select “Enroll MOK” before Linux boots the next time."
                        echo "d. Remember to enter the same password as the MOK."
                        echo "Driver installation will fail without the above steps."
                        echo "In case you miss any of these steps, you can also manually repeat the process later with the command “sudo update-secureboot-policy --enroll-key” and reboot."
                        echo "Ensure steps a, b, c and d."
                        echo "Resuming installation in a minute..."
                        sleep 60
                    fi
                    
                    #Additional Option for NuNet's GPU ML Use Case
                    mlCase()
                    {
                        echo "-----Starting Optional Machine Learning Framework Installation-----"
                        echo "Which of these Machine Learning(ML) Frameworks would you like to readily include with your onboarded GPU? (speeds up running ML related jobs on Nomad):"
                        echo "Note: To learn more about them, use their respective links below (for desktop, press & hold the Ctrl key and click to open on your default browser)."
                        echo "---------------------------------------------"
                        echo "1. PyTorch: https://pytorch.org"
                        echo "2. TensorFlow: https://tensorflow.org"
                        echo "3. Both 1 & 2"
                        echo "4. None"
                        echo "---------------------------------------------"
                        read ml;
                        case $ml in
                          1) sudo docker pull pytorch/pytorch:latest && echo "-----PyTorch Installed-----";;
                          2) sudo docker pull tensorflow/tensorflow:latest-gpu && echo "-----TensorFlow Installed-----";;
                          3) sudo docker pull pytorch/pytorch:latest && sudo docker pull tensorflow/tensorflow:latest-gpu && echo "-----Both PyTorch & TensorFlow Installed-----";;
                          4) sleep 0 && echo "-----Skipped PyTorch/TensorFlow Installation-----";; #Does nothing and skips ML framework inclusion
                          *) echo "Invalid option - you must choose among 1-4." && mlCase;;
                        esac
                    }                   

                    nvidia="$(lspci | grep 'VGA compatible controller: NVIDIA' |  awk '{print $5}')"
                    gpu="$(lspci | grep 'VGA compatible controller: NVIDIA' |  awk '{print $9 " "  $10 " "  $11}')"
                    gpu_driver="$(nvidia-smi | grep Driver |  awk '{print "NVIDIA " $4 " "  $5 " "  $6}')"
                    gpu_driver_var="$(nvidia-smi | grep Driver |  awk '{print $4}')"
                    # CHECK IF THERE IS NVIDIA GPU IF THERE IS NO NVIDIA GPU THE nvidia VARIABLE WILL BE EMPTY SO WE WONT DO ANYTHING
                    if [ -n "$nvidia" ]; then
                        echo "$nvidia $gpu GPU device found!"
                        # IF gpu_driver_var IS not EMPTY WE WILL SKIP DRIVER AND Install container runtime with PLUGIN
                        if [ -n "$gpu_driver_var" ]; then #IF $gpu_driver_var is not empty
                            echo "$gpu_driver is already installed. Skipping installation of driver."
                            
                            echo "-----Starting NVIDIA Container Runtime Installation for Nomad Plugin-----"
                            curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | sudo apt-key add -
                            distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
                            curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list | sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
                            sudo apt update
                            sudo apt install -y nvidia-container-runtime
                            #Backing up any pre-existing /etc/docker/daemon.json file before replacing it with a new one to specify the NVIDIA runtime for Nomad
                            docker_daemon=/etc/docker/daemon.json
                            if test -f "$docker_daemon"; then
                                echo "Backing up pre-existing /etc/docker/daemon.json file before replacing it with a new one to specify the NVIDIA runtime for Nomad"
                                sudo mv /etc/docker/daemon.json /etc/docker/daemon.json.backup                            
                                echo "Backup saved as /etc/docker/daemon.json.backup."
                            fi
                            echo "#Backed up during NuNet installation to specify NVIDIA container runtime for Nomad(before creating new daemon.json at /etc/docker)" | sudo tee -a /etc/docker/daemon.json.backup >/dev/null
                            echo -n "#Backup Date and Time: " | sudo tee -a /etc/docker/daemon.json.backup >/dev/null && date | sudo tee -a /etc/docker/daemon.json.backup >/dev/null
                            #Creates a new configuration for Docker to specify the NVIDIA runtime for Nomad
                            sudo cp configs/daemon.json /etc/docker
                            sudo systemctl restart docker
                            echo "-----NVIDIA Container Runtime Installed-----"
                            
                            
                            echo "-----Starting NVIDIA Nomad Plugin Installation-----"
                            wget https://releases.hashicorp.com/nomad-device-nvidia/1.0.0/nomad-device-nvidia_1.0.0_linux_amd64.zip
                            unzip nomad-device-nvidia_1.0.0_linux_amd64.zip
                            sudo mkdir /opt/nomad-plugin
                            sudo mv nomad-device-nvidia /opt/nomad-plugin
                            echo "-----NVIDIA Nomad Plugin Installed-----"
                            mlCase
                        else
                            echo " -----Starting NVIDIA GPU Driver Installation-----"
                            #sudo add-apt-repository ppa:graphics-drivers/ppa
                            sudo apt install -y ubuntu-drivers-common #package required for below command
                            sudo ubuntu-drivers install #autoinstall is deprecated
                            #sudo reboot
                            echo " -----NVIDIA GPU Driver Installed-----"
                            
                            echo "-----Starting NVIDIA Container Runtime Installation for Nomad Plugin-----"
                            curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | sudo apt-key add -
                            distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
                            curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list | sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
                            sudo apt update
                            sudo apt install -y nvidia-container-runtime
                            #Backing up any pre-existing /etc/docker/daemon.json file before replacing it with a new one to specify the NVIDIA runtime for Nomad
                            docker_daemon=/etc/docker/daemon.json
                            if test -f "$docker_daemon"; then
                                echo "Backing up pre-existing /etc/docker/daemon.json file before replacing it with a new one to specify the NVIDIA runtime for Nomad"
                                sudo mv /etc/docker/daemon.json /etc/docker/daemon.json.backup                            
                                echo "Backup saved as /etc/docker/daemon.json.backup."
                            fi
                            echo "#Backed up during NuNet installation to specify NVIDIA container runtime for Nomad(before creating new daemon.json at /etc/docker)" | sudo tee -a /etc/docker/daemon.json.backup >/dev/null
                            echo -n "#Backup Date and Time: " | sudo tee -a /etc/docker/daemon.json.backup >/dev/null && date | sudo tee -a /etc/docker/daemon.json.backup >/dev/null                           
                            #Creates a new configuration for Docker to specify the NVIDIA runtime for Nomad
                            sudo cp configs/daemon.json /etc/docker
                            sudo systemctl restart docker                      
                            echo "-----NVIDIA Container Runtime Installed-----"
                            
                            echo "-----Starting NVIDIA Nomad Plugin Installation-----"
                            wget https://releases.hashicorp.com/nomad-device-nvidia/1.0.0/nomad-device-nvidia_1.0.0_linux_amd64.zip
                            unzip nomad-device-nvidia_1.0.0_linux_amd64.zip
                            sudo mkdir /opt/nomad-plugin
                            sudo mv nomad-device-nvidia /opt/nomad-plugin
                            echo "-----NVIDIA Nomad Plugin Installed-----"
                            mlCase
                         
                        fi

                    fi

                    

                    end_log
                ;;

                arm64)
                    sudo apt install software-properties-common -y
                    sudo apt-add-repository universe
                    sudo apt update
                    sudo apt install python2-minimal jq -y
                    # nat
                    start_log
                    sudo apt-get -y install lsb-core
                    sudo apt install unzip
                    curl --version >/dev/null 2>&1
                    status=`echo $?`
                    if [[ $status -eq 0 ]]
                    then
                        echo "Curl is already installed on the system"
                    else
                        echo "Installing Curl"
                        sudo apt -y install  curl
                        echo "Installation Complete"
                    fi

                    nomad -v >/dev/null 2>&1
                    status=`echo $?`
                    if [[ $status -eq 0 ]]
                    then
                        echo "Nomad is already installed on the system"
                    else                
                        echo "-----Starting Nomad Installation-----"
                        wget -O /tmp/nomad.zip https://releases.hashicorp.com/nomad/1.1.6/nomad_1.1.6_linux_arm64.zip
                        cd /tmp
                        unzip nomad.zip
                        sudo mv nomad /usr/bin
                        sudo rm /tmp/nomad.zip
                        echo "Nomad installed successfully"
                    fi

                    docker -v >/dev/null 2>&1
                    status=`echo $?`
                    if [[ $status -eq 0 ]]
                    then 
                        echo "Docker is already installed on the system"
                    else
                        echo " -----Starting Docker Installation-----"
                        sudo apt-get purge docker-ce docker-ce-cli containerd.io
                        sudo apt-get update
                        sudo apt-get install -y apt-transport-https ca-certificates gnupg  
                        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
                        echo \
                        "deb [arch=$arch signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
                        $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
                        sudo apt-get update
                        sudo apt-get -y install docker-ce docker-ce-cli containerd.io
                        sudo groupadd docker
                        sudo usermod -aG docker $USER
                        newgrp docker
                        echo "Docker installed successfully" 
                    fi

                    

                    end_log
                ;;
            esac
        ;;
        kali)
            case "$arch" in

                amd64)
                    sudo apt update
                    sudo apt install software-properties-common -y
                    sudo apt update
                    sudo apt install python2-minimal jq -y
                    sudo apt-get update
                    # nat
                    start_log
                    sudo apt-get -y install lsb-core
                    curl --version >/dev/null 2>&1
                    status=`echo $?`
                    if [[ $status -eq 0 ]]
                    then
                        echo "Curl is already installed on the system"
                    else
                        echo "Installing Curl"
                        sudo apt -y install  curl
                        echo "Installation Complete"
                    fi

                    nomad -v >/dev/null 2>&1
                    status=`echo $?`
                    if [[ $status -eq 0 ]]
                    then
                        echo "Nomad is already installed on the system"
                    else
                        wget -O /tmp/nomad.zip https://releases.hashicorp.com/nomad/1.2.6/nomad_1.2.6_linux_amd64.zip
                        cd /tmp
                        unzip nomad.zip
                        sudo mv nomad /usr/bin
                        sudo rm /tmp/nomad.zip
                    fi

                    docker -v >/dev/null 2>&1
                    status=`echo $?`
                    if [[ $status -eq 0 ]]
                    then 
                        echo "Docker is already installed on the system"
                    else
                        echo " -----Starting Docker Installation-----"
                        sudo apt-get purge docker-ce docker-ce-cli containerd.io
                        sudo apt-get update
                        sudo apt-get install -y apt-transport-https ca-certificates gnupg  
                        printf '%s\n' "deb https://download.docker.com/linux/debian bullseye stable" | sudo tee /etc/apt/sources.list.d/docker-ce.list
                        curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/docker-ce-archive-keyring.gpg
                        sudo apt-get update
                        sudo apt install -y docker-ce docker-ce-cli containerd.io
                        sudo groupadd docker
                        sudo usermod -aG docker $USER
                        newgrp docker
                        
                        echo "Docker installed successfully" 
                    fi

                    #Check for Secure Boot before proceeding with GPU driver installation, alert user with necessary information if yes.
                    sudo apt install -y mokutil
                    secure_boot_check="$(mokutil --sb-state | grep enabled)"
                    secure_boot="enabled"
                    if grep -q "$secure_boot" <<< "$secure_boot_check"; then
                        echo "************Important Note!!!************"
                        echo "Your system has UEFI Secure Boot enabled."
                        echo "*****************************************"
                        echo "a. NVIDIA GPU Driver installation will require you to enroll a password(confirmed twice) as a machine owner key(MOK)."
                        echo "b. Reboot your system after finishing the onboarding procedure."
                        echo "c. Make sure you select “Enroll MOK” before Linux boots the next time."
                        echo "d. Remember to enter the same password as the MOK."
                        echo "Driver installation will fail without the above steps."
                        echo "In case you miss any of these steps, you can also manually repeat the process later with the command “sudo update-secureboot-policy --enroll-key” and reboot."
                        echo "Ensure steps a, b, c and d."
                        echo "Resuming installation in a minute..."
                        sleep 60
                    fi

                    #Additional Option for NuNet's GPU ML Use Case
                    mlCase()
                    {
                        echo "-----Starting Optional Machine Learning Framework Installation-----"
                        echo "Which of these Machine Learning(ML) Frameworks would you like to readily include with your onboarded GPU? (speeds up running ML related jobs on Nomad):"
                        echo "Note: To learn more about them, use their respective links below (for desktop, press & hold the Ctrl key and click to open on your default browser)."
                        echo "---------------------------------------------"
                        echo "1. PyTorch: https://pytorch.org"
                        echo "2. TensorFlow: https://tensorflow.org"
                        echo "3. Both 1 & 2"
                        echo "4. None"
                        echo "---------------------------------------------"
                        read ml;
                        case $ml in
                          1) sudo docker pull pytorch/pytorch:latest && echo "-----PyTorch Installed-----";;
                          2) sudo docker pull tensorflow/tensorflow:latest-gpu && echo "-----TensorFlow Installed-----";;
                          3) sudo docker pull pytorch/pytorch:latest && sudo docker pull tensorflow/tensorflow:latest-gpu && echo "-----Both PyTorch & TensorFlow Installed-----";;
                          4) sleep 0 && echo "-----Skipped PyTorch/TensorFlow Installation-----";; #Does nothing and skips ML framework inclusion
                          *) echo "Invalid option - you must choose among 1-4." && mlCase;;
                        esac
                    } 

                    nvidia="$(lspci | grep 'VGA compatible controller: NVIDIA' |  awk '{print $5}')"
                    gpu="$(lspci | grep 'VGA compatible controller: NVIDIA' |  awk '{print $9 " "  $10 " "  $11}')"
                    gpu_driver="$(nvidia-smi | grep Driver |  awk '{print "NVIDIA " $4 " "  $5 " "  $6}')"
                    gpu_driver_var="$(nvidia-smi | grep Driver |  awk '{print $4}')"
                    # CHECK IF THERE IS NVIDIA GPU IF THERE IS NO NVIDIA GPU THE nvidia VARIABLE WILL BE EMPTY SO WE WONT DO ANYTHING
                    if [ -n "$nvidia" ]; then
                        echo "$nvidia $gpu GPU device found!"
                        # IF gpu_driver_var IS not EMPTY WE WILL SKIP DRIVER AND Install container runtime with PLUGIN
                        if [ -n "$gpu_driver_var" ]; then #IF $gpu_driver_var is not empty
                            echo "$gpu_driver is already installed. Skipping installation of driver."
                            
                            echo "-----Starting NVIDIA Container Runtime Installation for Nomad Plugin-----"
                            curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | sudo apt-key add -
                            distribution=ubuntu18.04
                            curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list | sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
                            sudo apt update
                            sudo apt install -y nvidia-container-runtime
                            #Backing up any pre-existing /etc/docker/daemon.json file before replacing it with a new one to specify the NVIDIA runtime for Nomad
                            docker_daemon=/etc/docker/daemon.json
                            if test -f "$docker_daemon"; then
                                echo "Backing up pre-existing /etc/docker/daemon.json file before replacing it with a new one to specify the NVIDIA runtime for Nomad"
                                sudo mv /etc/docker/daemon.json /etc/docker/daemon.json.backup                            
                                echo "Backup saved as /etc/docker/daemon.json.backup."
                            fi
                            echo "#Backed up during NuNet installation to specify NVIDIA container runtime for Nomad(before creating new daemon.json at /etc/docker)" | sudo tee -a /etc/docker/daemon.json.backup >/dev/null
                            echo -n "#Backup Date and Time: " | sudo tee -a /etc/docker/daemon.json.backup >/dev/null && date | sudo tee -a /etc/docker/daemon.json.backup >/dev/null                            
                            #Creates a new configuration for Docker to specify the NVIDIA runtime for Nomad
                            sudo cp configs/daemon.json /etc/docker
                            sudo systemctl restart docker                           
                            echo "-----NVIDIA Container Runtime Installed-----"
                            
                            
                            echo "-----Starting NVIDIA Nomad Plugin Installation-----"
                            wget https://releases.hashicorp.com/nomad-device-nvidia/1.0.0/nomad-device-nvidia_1.0.0_linux_amd64.zip
                            unzip nomad-device-nvidia_1.0.0_linux_amd64.zip
                            sudo mkdir /opt/nomad-plugin
                            sudo mv nomad-device-nvidia /opt/nomad-plugin
                            echo "-----NVIDIA Nomad Plugin Installed-----"
                            mlCase
                        else
                            echo " -----Starting NVIDIA GPU Driver Installation-----"
                            sudo apt install update && sudo apt -y full-upgrade && sudo apt -y autoremove
                            #sudo reboot                            
                            sudo apt install -y nvidia-driver
                            echo " -----NVIDIA GPU Driver Installed-----"
                            
                            echo "-----Starting NVIDIA Container Runtime Installation for Nomad Plugin-----"
                            curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | sudo apt-key add -
                            distribution=ubuntu18.04
                            curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list | sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
                            sudo apt update
                            sudo apt install -y nvidia-container-runtime
                            #Backing up any pre-existing /etc/docker/daemon.json file before replacing it with a new one to specify the NVIDIA runtime for Nomad
                            docker_daemon=/etc/docker/daemon.json
                            if test -f "$docker_daemon"; then
                                echo "Backing up pre-existing /etc/docker/daemon.json file before replacing it with a new one to specify the NVIDIA runtime for Nomad"
                                sudo mv /etc/docker/daemon.json /etc/docker/daemon.json.backup                            
                                echo "Backup saved as /etc/docker/daemon.json.backup."
                            fi
                            echo "#Backed up during NuNet installation to specify NVIDIA container runtime for Nomad(before creating new daemon.json at /etc/docker)" | sudo tee -a /etc/docker/daemon.json.backup >/dev/null
                            echo -n "#Backup Date and Time: " | sudo tee -a /etc/docker/daemon.json.backup >/dev/null && date | sudo tee -a /etc/docker/daemon.json.backup >/dev/null                           
                            #Creates a new configuration for Docker to specify the NVIDIA runtime for Nomad
                            sudo cp configs/daemon.json /etc/docker
                            sudo systemctl restart docker                                                       
                            echo "-----NVIDIA Container Runtime Installed-----"
                            
                            echo "-----Starting NVIDIA Nomad Plugin Installation-----"
                            wget https://releases.hashicorp.com/nomad-device-nvidia/1.0.0/nomad-device-nvidia_1.0.0_linux_amd64.zip
                            unzip nomad-device-nvidia_1.0.0_linux_amd64.zip
                            sudo mkdir /opt/nomad-plugin
                            sudo mv nomad-device-nvidia /opt/nomad-plugin
                            echo "-----NVIDIA Nomad Plugin Installed-----"
                            mlCase
                         
                        fi

                    fi

                    

                    end_log
                    ;;
            esac
        ;;


        raspbian)
            sudo apt install python-minimal jq -y
            # nat
            start_log
            
            sudo apt install unzip
            wget -O /tmp/libseccomp.deb http://ftp.us.debian.org/debian/pool/main/libs/libseccomp/libseccomp2_2.5.1-1_armhf.deb
            sudo dpkg -i /tmp/libseccomp.deb
            sudo rm /tmp/libseccomp.deb

            curl --version >/dev/null 2>&1
            status=`echo $?`
            if [[ $status -eq 0 ]]
            then
                echo "Curl is already installed on the system"
            else
                echo "Installing Curl"
                sudo apt -y install  curl
                echo "Installation Complete"
            fi

            nomad -v >/dev/null 2>&1
            status=`echo $?`
            if [[ $status -eq 0 ]]
            then
                echo "Nomad is already installed on the system"
            else                
                echo "-----Starting Nomad Installation-----"
                wget -O /tmp/nomad.zip https://releases.hashicorp.com/nomad/1.1.4/nomad_1.1.4_linux_arm.zip
                cd /tmp
                unzip nomad.zip
                sudo mv nomad /usr/bin
                sudo rm /tmp/nomad.zip

                echo "Nomad installed successfully"
            fi

            docker -v >/dev/null 2>&1
            status=`echo $?`
            if [[ $status -eq 0 ]]
            then 
                echo "Docker is already installed on the system"
            else
                echo " -----Starting Docker Installation-----"
                sudo apt-get update
                curl -fsSL "https://download.docker.com/linux/raspbian/gpg" | sudo gpg --dearmor --yes -o /usr/share/keyrings/docker-archive-keyring.gpg
                echo \
                "deb [arch=armhf signed-by=/usr/share/keyrings/docker-archive-keyring.gpg]\
                https://download.docker.com/linux/raspbian buster stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
                sudo apt-get update
                sudo apt-get install -y --no-install-recommends docker-ce docker-ce-cli containerd.io

                sudo groupadd docker
                sudo usermod -aG docker $USER
                sleep 10
                sudo dpkg --configure -a
                newgrp docker

                echo "Docker installed successfully"
            fi
            

            end_log
        ;;

            

    esac
    
    
}
nat(){
    echo "Detecting NAT-type..."
    output=$(python2 natdetector.py nat)
    if [[ $output -eq 1 ]]
    then
        echo "Sorry, we're not able to onboard your device as the platform currently doesn't support Symmetric NAT"
        exit 1
    else
       : 
    fi
}


start_log()
{
    if [ ! -d /var/log/nunet-log ]; then
        sudo mkdir -p /var/log/nunet-log/
        sudo touch /var/log/nunet-log/start.log
    fi
    CLIENT_DIR="/var/log/nunet-log"
    if [ ! -d $CLIENT_DIR ]; then
        sudo mkdir -p $CLIENT_DIR setfacl -Rm g:MY_GROUP_ID:rwx /var/log/my_app/
    fi

    if [ ! -d /var/log/nunet-log ]; then
        sudo mkdir -p /var/log/nunet-log
        sudo touch $CLIENT_DIR/start.log
        sudo touch $CLIENT_DIR/end.log
    fi
    less /var/log/apt/history.log | sed -n -e 's/^.*Commandline: apt-get .* install //p' | sudo tee -a $CLIENT_DIR/start.log >/dev/null && \
    less /var/log/apt/history.log | sed -n -e 's/^.*Commandline: apt-get install //p' | sudo tee -a $CLIENT_DIR/start.log >/dev/null && \
    less /var/log/apt/history.log | sed -n -e 's/^.*Commandline: apt install //p' | sudo tee -a $CLIENT_DIR/start.log >/dev/null && \
    less /var/log/apt/history.log | sed -n -e 's/^.*Commandline: apt .* install //p' | sudo tee -a $CLIENT_DIR/start.log >/dev/null
}

end_log()
{   
    CLIENT_DIR="/var/log/nunet-log"
    if [  -d /var/log/nunet-log ]; then
        sudo touch $CLIENT_DIR/end.log
    fi
    less /var/log/apt/history.log | sed -n -e 's/^.*Commandline: apt-get .* install //p' | sudo tee -a $CLIENT_DIR/end.log >/dev/null && \
    less /var/log/apt/history.log | sed -n -e 's/^.*Commandline: apt-get install //p' | sudo tee -a $CLIENT_DIR/end.log >/dev/null && \
    less /var/log/apt/history.log | sed -n -e 's/^.*Commandline: apt install //p' | sudo tee -a $CLIENT_DIR/end.log >/dev/null && \
    less /var/log/apt/history.log | sed -n -e 's/^.*Commandline: apt .* install //p' | sudo tee -a $CLIENT_DIR/end.log >/dev/null
}

unpause()
{
    echo "Connecting your device back to NuNet. Please standby for 20 seconds..."
    sudo systemctl enable nomad-client.service
    sudo systemctl start nomad-client.service    
    sleep 20
    echo "Done!"
}

pause()
{
    echo "Disconnecting your device from NuNet. Please standby for 10 seconds..."
    sudo systemctl stop nomad-client.service
    sudo systemctl disable nomad-client.service    
    sleep 10
    echo "Done!"
}

remove()
{   
    lsb_dist=$( get_distribution )
    lsb_dist="$(echo "$lsb_dist" | tr '[:upper:]' '[:lower:]')"

    case "$lsb_dist" in

        ubuntu)
            echo "Stopping nomad client ...."
            spinner &
            SPIN_PID=$!
            sudo systemctl stop nomad-client.service
            kill $SPIN_PID
            wait $SPIN_PID 2>/dev/null
            echo "Removing nomad client ...."
            sudo systemctl disable nomad-client.service
            sudo rm /etc/systemd/system/nomad-client.service
            sudo systemctl daemon-reload
            docker rm -f $(docker ps -aqf "name=$(hostname)") >/dev/null
            echo "Nomad client removed successfully"
            CLIENT_DIR="/var/log/nunet-log"
            if [  -d /var/log/nunet-log ]; then
                sudo touch $CLIENT_DIR/installed-packages.log
                fi
            diff $CLIENT_DIR/start.log $CLIENT_DIR/end.log | tail -n +2 | sed -e '/^>/!d' |\
            cut -d'>' -f2 | sudo tee -a $CLIENT_DIR/installed-packages.log >/dev/null
            sudo xargs -a $CLIENT_DIR/installed-packages.log apt-get remove --purge
            
            echo "Do you want to remove configuration files? (Y/N)"
            read input
            if [ "$input" = "Y" ] || [ "$input" = "y" ]; then
                sudo rm -rf /etc/nunet
                echo "Configuration files removed successfully!"
                fi
            sudo rm -rf $CLIENT_DIR

            # remove daemon.json created earlier to specify nvidia runtime for nomad
            echo "Removing Nomad specific NVIDIA Runtime Configuration /etc/docker/daemon.json from Docker ...."
            sudo rm /etc/docker/daemon.json
            echo "Configuration removed successfully"
            # restore original daemon config(if any) from backup
            docker_daemon_backup=/etc/docker/daemon.json.backup
            if test -f "$docker_daemon_backup"; then
                echo "Restoring original Docker /etc/docker/daemon.json configuration from backup ...."
                sudo cp /etc/docker/daemon.json.backup /etc/docker/daemon.json
                echo -n "#Restoration Date and Time: " | sudo tee -a /etc/docker/daemon.json >/dev/null && date | sudo tee -a /etc/docker/daemon.json >/dev/null
                echo "Backup restored successfully"
            fi

            # remove nomad plugin if installed           
            if [ -d "/opt/nomad-plugin" ]; then sudo rm -Rf /opt/nomad-plugin; fi
        ;;
        kali)
            echo "Stopping nomad client ...."
            spinner &
            SPIN_PID=$!
            sudo systemctl stop nomad-client.service
            kill $SPIN_PID
            wait $SPIN_PID 2>/dev/null
            echo "Removing nomad client ...."
            sudo systemctl disable nomad-client.service
            sudo rm /etc/systemd/system/nomad-client.service
            sudo systemctl daemon-reload
            docker rm -f $(docker ps -aqf "name=$(hostname)") >/dev/null
            echo "Nomad client removed successfully"
            sudo rm /usr/bin/nomad
            CLIENT_DIR="/var/log/nunet-log"
            if [  -d /var/log/nunet-log ]; then
                sudo touch $CLIENT_DIR/installed-packages.log
                fi
            diff $CLIENT_DIR/start.log $CLIENT_DIR/end.log | tail -n +2 | sed -e '/^>/!d' |\
            cut -d'>' -f2 | sudo tee -a $CLIENT_DIR/installed-packages.log >/dev/null
            sudo xargs -a $CLIENT_DIR/installed-packages.log apt-get remove --purge
            
            echo "Do you want to remove configuration files? (Y/N)"
            read input
            if [ "$input" = "Y" ] || [ "$input" = "y" ]; then
                sudo rm -rf /etc/nunet
                echo "Configuration files removed successfully!"
                fi
            sudo rm -rf $CLIENT_DIR
                        
            # remove daemon.json created earlier to specify nvidia runtime for nomad
            echo "Removing Nomad specific NVIDIA Runtime Configuration /etc/docker/daemon.json from Docker ...."
            sudo rm /etc/docker/daemon.json
            echo "Configuration removed successfully"
            # restore original daemon config(if any) from backup
            docker_daemon_backup=/etc/docker/daemon.json.backup
            if test -f "$docker_daemon_backup"; then
                echo "Restoring original Docker /etc/docker/daemon.json configuration from backup ...."
                sudo cp /etc/docker/daemon.json.backup /etc/docker/daemon.json
                echo -n "#Restoration Date and Time: " | sudo tee -a /etc/docker/daemon.json >/dev/null && date | sudo tee -a /etc/docker/daemon.json >/dev/null
                echo "Backup restored successfully"
            fi
            
            # remove nomad plugin if installed 
            if [ -d "/opt/nomad-plugin" ]; then sudo rm -Rf /opt/nomad-plugin; fi
        ;;
        raspbian)
            echo "Stopping nomad client ...."
            spinner &
            SPIN_PID=$!
            sudo systemctl stop nomad-client.service
            kill $SPIN_PID
            wait $SPIN_PID 2>/dev/null
            echo "Removing nomad client ...."
            sudo systemctl disable nomad-client.service
            sudo rm /etc/systemd/system/nomad-client.service
            sudo systemctl daemon-reload
            docker rm -f $(docker ps -aqf "name=$(hostname)") >/dev/null
            echo "Nomad client removed successfully"
            sudo rm /usr/bin/nomad
            if [  -d /var/log/nunet-log ]; then
                sudo touch $CLIENT_DIR/installed-packages.log
            fi
            CLIENT_DIR="/var/log/nunet-log"
            diff $CLIENT_DIR/start.log $CLIENT_DIR/end.log | tail -n +2 | sed -e '/^>/!d' |\
            cut -d'>' -f2 | sudo tee -a $CLIENT_DIR/installed-packages.log >/dev/null
            sudo xargs -a $CLIENT_DIR/installed-packages.log apt-get remove --purge
            echo "Do you want to remove configuration files? (Y/N)"
            read input
            if [ "$input" = "Y" ] || [ "$input" = "y" ]; then
                sudo rm -rf /etc/nunet
                echo "Configuration files removed successfully!"
                fi
            sudo rm -rf $CLIENT_DIR

        ;;
    esac
}

config() {
   lsb_dist=$( get_distribution )
    lsb_dist="$(echo "$lsb_dist" | tr '[:upper:]' '[:lower:]')"

    case "$lsb_dist" in

        ubuntu)
            # nat
            sudo systemctl stop nomad-client.service >/dev/null 2>&1
            sudo systemctl disable nomad-client.service >/dev/null 2>&1
            CLIENT_DIR="/etc/nunet"
            if [ ! -d $CLIENT_DIR ]; then
                sudo mkdir -p $CLIENT_DIR
            fi
            sudo docker build -t onboarding:latest .
            clear
            sudo docker run --network host -v /etc/nunet:/onboarding/configs/client_config -v /home/nunet:/onboarding/private -e "HOSTNAME=$(cat /etc/hostname)" -e "OS_RELEASE=$lsb_dist" -it onboarding:latest

            #Beautify json config files
            sudo jq . /etc/nunet/adapter-definition.json > /etc/nunet/temp-adapter-definition.json
            sudo jq . /etc/nunet/client.json > /etc/nunet/temp-client.json
            sudo jq . /etc/nunet/metadata.json > /etc/nunet/temp-metadata.json 
            sudo mv /etc/nunet/temp-adapter-definition.json /etc/nunet/adapter-definition.json
            sudo mv /etc/nunet/temp-client.json /etc/nunet/client.json                                   
            sudo mv /etc/nunet/temp-metadata.json /etc/nunet/metadata.json
            
            sudo cp ./configs/nomad-client.service /etc/systemd/system/

            sudo systemctl enable nomad-client.service
            sudo systemctl start nomad-client.service
            
            JOB_NAME=$(head -n 1 /etc/nunet/JOB_NAME.txt)
            sudo curl -XPUT -d @$CLIENT_DIR/adapter-definition.json http://nomad.nunet.io:4646/v1/job/$JOB_NAME | json_pp
        ;;
        kali)
            # nat
            sudo systemctl stop nomad-client.service >/dev/null 2>&1
            sudo systemctl disable nomad-client.service >/dev/null 2>&1
            CLIENT_DIR="/etc/nunet"
            if [ ! -d $CLIENT_DIR ]; then
                sudo mkdir -p $CLIENT_DIR
            fi
            sudo docker build -t onboarding:latest .
            clear
            sudo docker run --network host -v /etc/nunet:/onboarding/configs/client_config -v /home/nunet:/onboarding/private -e "HOSTNAME=$(cat /etc/hostname)" -e "OS_RELEASE=$lsb_dist" -it onboarding:latest

            #Beautify json config files
            sudo jq . /etc/nunet/adapter-definition.json > /etc/nunet/temp-adapter-definition.json
            sudo jq . /etc/nunet/client.json > /etc/nunet/temp-client.json
            sudo jq . /etc/nunet/metadata.json > /etc/nunet/temp-metadata.json 
            sudo mv /etc/nunet/temp-adapter-definition.json /etc/nunet/adapter-definition.json
            sudo mv /etc/nunet/temp-client.json /etc/nunet/client.json                                   
            sudo mv /etc/nunet/temp-metadata.json /etc/nunet/metadata.json            
            sudo cp ./configs/nomad-client.service /etc/systemd/system/

            sudo systemctl enable nomad-client.service
            sudo systemctl start nomad-client.service
            
            JOB_NAME=$(head -n 1 /etc/nunet/JOB_NAME.txt)
            sudo curl -XPUT -d @$CLIENT_DIR/adapter-definition.json http://nomad.nunet.io:4646/v1/job/$JOB_NAME | json_pp
        ;;
        raspbian)
            # nat
            sudo systemctl stop nomad-client.service >/dev/null 2>&1
            sudo systemctl disable nomad-client.service >/dev/null 2>&1
            CLIENT_DIR="/etc/nunet"
            if [ ! -d $CLIENT_DIR ]; then
                sudo mkdir -p $CLIENT_DIR
            fi
            sudo docker build -t onboarding:latest .
            clear
            sudo docker run --network host -v /etc/nunet:/onboarding/configs/client_config -v /home/nunet:/onboarding/private -e "HOSTNAME=$(cat /etc/hostname)" -e "OS_RELEASE=$lsb_dist" -it onboarding:latest

            #Beautify json config files
            sudo jq . /etc/nunet/adapter-definition.json > /etc/nunet/temp-adapter-definition.json
            sudo jq . /etc/nunet/client.json > /etc/nunet/temp-client.json
            sudo jq . /etc/nunet/metadata.json > /etc/nunet/temp-metadata.json 
            sudo mv /etc/nunet/temp-adapter-definition.json /etc/nunet/adapter-definition.json
            sudo mv /etc/nunet/temp-client.json /etc/nunet/client.json                                   
            sudo mv /etc/nunet/temp-metadata.json /etc/nunet/metadata.json            
            sudo cp ./configs/nomad-client.service /etc/systemd/system/

            sudo systemctl enable nomad-client.service
            sudo systemctl start nomad-client.service
            
            JOB_NAME=$(head -n 1 /etc/nunet/JOB_NAME.txt)
            sudo curl -XPUT -d @$CLIENT_DIR/adapter-definition.json http://nomad.nunet.io:4646/v1/job/$JOB_NAME | json_pp
        ;;
    esac

}

spinner()
{
  spinner="/|\\-/|\\-"
  while :
  do
    for i in `seq 0 7`
    do
      echo -n "${spinner:$i:1}"
      echo -en "\010"
      sleep 0.3
    done
  done
}


all() {
    install
    config
}

case $1 in 
    config) config;;
    install) install;;
    unpause) unpause;;
    pause) pause;;
    remove) remove;;
    start_log) start_log;;
    end_log) end_log;;
    nat) nat;;
    spinner) spinner;;
    get_distribution) get_distribution;;
    get_architecture) get_architecture;;
    all) all;;
esac
