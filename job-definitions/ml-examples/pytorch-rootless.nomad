job "pytorch-gpu" {
  datacenters = ["nunet-private-alpha"]
  group "pytorch-gpu" {
    task "pytorch-gpu-docker" {
      user = "10100:10100"
      driver = "docker"
      config {
        image = "pytorch/pytorch:latest"
        interactive = true
      }
      resources {
        cpu    = 500
        memory = 2048
        device "nvidia/gpu" {
          count = 1
        }
      }
    }
  }
}
