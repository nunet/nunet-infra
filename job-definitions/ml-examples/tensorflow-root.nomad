job "tensorflow-gpu" {
  datacenters = ["nunet-private-alpha"]
  group "tensorflow-gpu" {
    task "tensorflow-gpu-docker" {
      driver = "docker"
      config {
        image = "tensorflow/tensorflow:latest-gpu"
        interactive = true
      }
      resources {
        cpu    = 500
        memory = 2048
        device "nvidia/gpu" {
          count = 1
        }
      }
    }
  }
}
