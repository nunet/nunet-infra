# Provisioning

This directory contains infrastructure-as-code (IaC) configurations and CI/CD pipeline templates for provisioning various resources using OpenTofu.

## Structure

- `gitlab-secrets/` - Manages GitLab group access tokens and variables
- `templates/` - Reusable GitLab CI pipeline templates

## Using the full-pipeline Component

The `full-pipeline` component provides a standardized OpenTofu pipeline that includes stages for formatting, validation, testing, planning, and applying infrastructure changes.

### Example Usage

1. Create your OpenTofu configuration files in a subdirectory:

```hcl
# your-project/provider.tf
provider "gitlab" {}

# your-project/main.tf
resource "gitlab_project" "example" {
  name = "example-project"
}
```

2. Create a pipeline file that includes the full-pipeline component:

```yaml
# your-project/Main.gitlab-ci.yml
include:
  - local: provisioning/templates/full-pipeline.gitlab-ci.yml
    inputs:
      root_dir: path/to/your-project
```

3. Include your project's pipeline in the root Index.gitlab-ci.yml:

```yaml
your_project:
  trigger:
    include:
      - local: path/to/your-project/Main.gitlab-ci.yml
  rules:
    - when: always
```

# Execution

To execute the provisioning pipeline, create a new web pipeline including the environment variable
`CI_PATHWAY_TO_TRIGGER=provisioning`

