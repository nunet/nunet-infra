data "gitlab_group" "nunet" {
  full_path = "nunet"
}

resource "time_rotating" "token_rotation" {
  rotation_days = 7
}

resource "gitlab_group_access_token" "ci_user" {
  group        = data.gitlab_group.nunet.group_id
  name         = "CI User ${time_rotating.token_rotation.rfc3339}"
  expires_at   = formatdate("YYYY-MM-DD", timeadd(timestamp(), "230h")) # a bit over 9 days
  access_level = "maintainer"

  scopes = ["api", "read_api", "read_repository", "write_repository", "read_registry", "write_registry"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "gitlab_group_variable" "gitlab_access_token" {
  group       = data.gitlab_group.nunet.group_id
  key         = "GITLAB_ACCESS_TOKEN"
  description = "GitLab Access Token for use within the CI/CD pipeline"
  value       = gitlab_group_access_token.ci_user.token

  masked    = true
  protected = false
  raw       = true
}
