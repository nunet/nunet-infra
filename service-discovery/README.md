# Global Registry

This is a centralized registy containing information of all services running and their respective endpoints. Each adapter queries this global registry with the required service name and the registry returns a list of endpoints of the requested service. 

## Build

Global registry compiles from the separate repository: https://gitlab.com/nunet/global-orchestrator.

## Start

`docker run -p 4556:4556 -dt registry.gitlab.com/nunet/global-orchestrator/service_discovery:latest `
